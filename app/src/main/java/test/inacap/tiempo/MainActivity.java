package test.inacap.tiempo;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;
import org.json.JSONException;

public class MainActivity extends AppCompatActivity {
    private TextView Chillan, Santiago, Concepcion, Madrid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.Chillan = (TextView) findViewById(R.id.Chillan);
        this.Santiago = (TextView) findViewById(R.id.Santiago);
        this.Concepcion = (TextView) findViewById(R.id.Concepcion);
        this.Madrid = (TextView)findViewById(R.id.Madrid);


        String chillanurl = "http://api.openweathermap.org/data/2.5/weather?lat=-36.6066399&lon=-72.1034393&appid=2163ce7f9e1fc3af51e709f3319dfd32&units=metric";

        StringRequest chillan = new StringRequest(

                Request.Method.GET,
                chillanurl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Tenemos respuesta desde el servidor
                        try {
                            JSONObject respuestaJSON = new JSONObject(response);


                            JSONObject tempJSON = respuestaJSON.getJSONObject("main");
                            double temp = tempJSON.getDouble("temp");
                            double pressure = tempJSON.getDouble("pressure");
                            double humidity = tempJSON.getDouble("humidity");
                            Chillan.setText("La temperatura en Chillán es: " + temp + "C°" + " La presión es: "+ pressure + " La humedad es: " + humidity);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Algo fallo
                    }
                }
        );

        RequestQueue listaEsperaChillan = Volley.newRequestQueue(getApplicationContext());
        listaEsperaChillan.add(chillan);


        String santiagourl = "http://api.openweathermap.org/data/2.5/weather?lat=-33.4726900&lon=-70.6472400&appid=2163ce7f9e1fc3af51e709f3319dfd32&units=metric";

        StringRequest santiago = new StringRequest(

                Request.Method.GET,
                santiagourl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Tenemos respuesta desde el servidor
                        try {
                            JSONObject respuestaJSON = new JSONObject(response);


                            JSONObject tempJSON = respuestaJSON.getJSONObject("main");
                            double temp = tempJSON.getDouble("temp");
                            double pressure = tempJSON.getDouble("pressure");
                            double humidity = tempJSON.getDouble("humidity");

                            Santiago.setText("La temperatura en Santiago es: " + temp + "C°" + " La presión es: " + pressure + " La humedad es: "+ humidity);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Algo fallo
                    }
                }
        );

        RequestQueue listaEsperaSantiago = Volley.newRequestQueue(getApplicationContext());
        listaEsperaSantiago.add(santiago);


        String concepcionurl = "http://api.openweathermap.org/data/2.5/weather?lat=-36.8269882&lon=-73.0497665&appid=2163ce7f9e1fc3af51e709f3319dfd32&units=metric";

        StringRequest concepcion = new StringRequest(

                Request.Method.GET,
                concepcionurl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Tenemos respuesta desde el servidor
                        try {
                            JSONObject respuestaJSON = new JSONObject(response);


                            JSONObject tempJSON = respuestaJSON.getJSONObject("main");
                            double temp = tempJSON.getDouble("temp");
                            double pressure = tempJSON.getDouble("pressure");
                            double humidity = tempJSON.getDouble("humidity");

                            Concepcion.setText("La temperatura en Concepción es: " + temp + "C°" + " La presión es: " + pressure + " La humedad es: "+ humidity);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Algo fallo
                    }
                }
        );

        RequestQueue listaEsperaConcepcion = Volley.newRequestQueue(getApplicationContext());
        listaEsperaConcepcion.add(concepcion);


        String madridurl = "http://api.openweathermap.org/data/2.5/weather?lat=40.4165000&lon=-3.7025600&appid=2163ce7f9e1fc3af51e709f3319dfd32&units=metric";

        StringRequest madrid = new StringRequest(

                Request.Method.GET,
                madridurl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Tenemos respuesta desde el servidor
                        try {
                            JSONObject respuestaJSON = new JSONObject(response);


                            JSONObject tempJSON = respuestaJSON.getJSONObject("main");
                            double temp = tempJSON.getDouble("temp");
                            double pressure = tempJSON.getDouble("pressure");
                            double humidity = tempJSON.getDouble("humidity");

                            Madrid.setText("La temperatura en Madrid, España es: " + temp + "C°" + " La presión es: " + pressure + " La humedad es: "+ humidity);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Algo fallo
                    }
                }
        );

        RequestQueue listaEsperaMadrid = Volley.newRequestQueue(getApplicationContext());
        listaEsperaMadrid.add(madrid);


    }
}
